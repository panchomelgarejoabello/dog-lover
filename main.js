document.addEventListener('DOMContentLoaded',() => {
    const e = document.querySelectorAll('.carousel');
    M.Carousel.init(e,{
        duration: 300,
        dist: 0,
        shift: 5,
        padding: 5,
        numVisible: 3,
        indicators: true,
        noWrap: false
    });
});